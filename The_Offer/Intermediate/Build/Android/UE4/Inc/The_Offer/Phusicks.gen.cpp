// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "The_Offer/Public/Phusicks.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhusicks() {}
// Cross Module References
	THE_OFFER_API UClass* Z_Construct_UClass_UPhusicks_NoRegister();
	THE_OFFER_API UClass* Z_Construct_UClass_UPhusicks();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_The_Offer();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UPhusicks_Jump_C();
// End Cross Module References
	void UPhusicks::StaticRegisterNativesUPhusicks()
	{
		UClass* Class = UPhusicks::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Jump_C", &UPhusicks::execJump_C },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPhusicks_Jump_C_Statics
	{
		struct Phusicks_eventJump_C_Parms
		{
			float speed;
			float time;
			float start_Z;
			float now_Z;
			float position_Z;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_position_Z;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_Z;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_start_Z;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_time;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_speed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Phusicks_eventJump_C_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Phusicks_eventJump_C_Parms), &Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_position_Z = { "position_Z", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Phusicks_eventJump_C_Parms, position_Z), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_now_Z = { "now_Z", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Phusicks_eventJump_C_Parms, now_Z), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_start_Z = { "start_Z", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Phusicks_eventJump_C_Parms, start_Z), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_time = { "time", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Phusicks_eventJump_C_Parms, time), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_speed = { "speed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Phusicks_eventJump_C_Parms, speed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPhusicks_Jump_C_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_position_Z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_now_Z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_start_Z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_time,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPhusicks_Jump_C_Statics::NewProp_speed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPhusicks_Jump_C_Statics::Function_MetaDataParams[] = {
		{ "Category", "Custom_Phusicks" },
		{ "Kewords", "Jump_C" },
		{ "ModuleRelativePath", "Public/Phusicks.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPhusicks_Jump_C_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPhusicks, nullptr, "Jump_C", nullptr, nullptr, sizeof(Phusicks_eventJump_C_Parms), Z_Construct_UFunction_UPhusicks_Jump_C_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPhusicks_Jump_C_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPhusicks_Jump_C_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPhusicks_Jump_C_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPhusicks_Jump_C()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPhusicks_Jump_C_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPhusicks_NoRegister()
	{
		return UPhusicks::StaticClass();
	}
	struct Z_Construct_UClass_UPhusicks_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhusicks_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_The_Offer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPhusicks_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPhusicks_Jump_C, "Jump_C" }, // 1637090387
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhusicks_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Phusicks.h" },
		{ "ModuleRelativePath", "Public/Phusicks.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhusicks_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhusicks>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhusicks_Statics::ClassParams = {
		&UPhusicks::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPhusicks_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhusicks_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhusicks()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhusicks_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhusicks, 2912086976);
	template<> THE_OFFER_API UClass* StaticClass<UPhusicks>()
	{
		return UPhusicks::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhusicks(Z_Construct_UClass_UPhusicks, &UPhusicks::StaticClass, TEXT("/Script/The_Offer"), TEXT("UPhusicks"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhusicks);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
