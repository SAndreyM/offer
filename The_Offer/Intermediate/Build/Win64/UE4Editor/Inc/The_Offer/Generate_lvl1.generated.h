// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef THE_OFFER_Generate_lvl1_generated_h
#error "Generate_lvl1.generated.h already included, missing '#pragma once' in Generate_lvl1.h"
#endif
#define THE_OFFER_Generate_lvl1_generated_h

#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_SPARSE_DATA
#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChooseChest) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Seed); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_out_chest); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::ChooseChest(Z_Param_Seed,Z_Param_Out_out_chest); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerStandart) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_z); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_coof_hard); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_block); \
		P_GET_PROPERTY(UIntProperty,Z_Param_cout_up_down); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_now_z_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_block_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_up_down_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_object); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerStandart(Z_Param_Seed,Z_Param_now_z,Z_Param_coof_hard,Z_Param_now_block,Z_Param_cout_up_down,Z_Param_Out_now_z_out,Z_Param_Out_now_block_out,Z_Param_Out_cout_up_down_out,Z_Param_Out_type_object); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerseparator) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_z); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_z_top); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_coof_hard); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_block); \
		P_GET_PROPERTY(UIntProperty,Z_Param_type_separator); \
		P_GET_PROPERTY(UIntProperty,Z_Param_cout_up_down_top); \
		P_GET_PROPERTY(UIntProperty,Z_Param_cout_up_down); \
		P_GET_PROPERTY(UIntProperty,Z_Param_expect); \
		P_GET_PROPERTY(UIntProperty,Z_Param_back_separator); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_top); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_down); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_block_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_separator_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_up_down_top_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_up_down_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_downPlat); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ts_platform_top); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ts_platform_down); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_expect_out); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::Generseparator(Z_Param_seed,Z_Param_now_z,Z_Param_now_z_top,Z_Param_coof_hard,Z_Param_now_block,Z_Param_type_separator,Z_Param_cout_up_down_top,Z_Param_cout_up_down,Z_Param_expect,Z_Param_back_separator,Z_Param_Out_z_top,Z_Param_Out_z_down,Z_Param_Out_now_block_out,Z_Param_Out_type_separator_out,Z_Param_Out_cout_up_down_top_out,Z_Param_Out_cout_up_down_out,Z_Param_Out_cout_downPlat,Z_Param_Out_ts_platform_top,Z_Param_Out_ts_platform_down,Z_Param_Out_expect_out); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerAngel) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_segment); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_position); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_position_top); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_top_position); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_down_top); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_down_position); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_object_top); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_object_down); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_segment_out); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerAngel(Z_Param_now_segment,Z_Param_now_position,Z_Param_now_position_top,Z_Param_Out_top_position,Z_Param_Out_down_top,Z_Param_Out_down_position,Z_Param_Out_type_object_top,Z_Param_Out_type_object_down,Z_Param_Out_now_segment_out); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenBaseSituation) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_object); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_vert); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_block); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_part_object); \
		P_GET_PROPERTY(UIntProperty,Z_Param_len); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_pos_top); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_len_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_spawn_object); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_object_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_part_object_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_block_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_gener_down); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenBaseSituation(Z_Param_seed,Z_Param_now_object,Z_Param_now_vert,Z_Param_now_block,Z_Param_now_part_object,Z_Param_len,Z_Param_Out_pos_top,Z_Param_Out_len_out,Z_Param_Out_type_spawn_object,Z_Param_Out_now_object_out,Z_Param_Out_now_part_object_out,Z_Param_Out_now_block_out,Z_Param_Out_gener_down); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenMoneyLvl1) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_z); \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_chance); \
		P_GET_UBOOL_REF(Z_Param_Out_money); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_money); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenMoneyLvl1(Z_Param_z,Z_Param_seed,Z_Param_chance,Z_Param_Out_money,Z_Param_Out_z_money); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerateLvl1_Ground) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_verh); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_niz); \
		P_GET_PROPERTY(UIntProperty,Z_Param_min_H); \
		P_GET_PROPERTY(UIntProperty,Z_Param_max_H); \
		P_GET_PROPERTY(UIntProperty,Z_Param_min_L); \
		P_GET_PROPERTY(UIntProperty,Z_Param_max_L); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_ground); \
		P_GET_PROPERTY(UIntProperty,Z_Param_stop_ground); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_lava); \
		P_GET_PROPERTY(UIntProperty,Z_Param_stop_lava); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_S); \
		P_GET_PROPERTY(UIntProperty,Z_Param_stop_S); \
		P_GET_PROPERTY(UIntProperty,Z_Param_coof_ground); \
		P_GET_PROPERTY(UIntProperty,Z_Param_iter); \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_z); \
		P_GET_UBOOL(Z_Param_S_en); \
		P_GET_UBOOL(Z_Param_L_en); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_verh_exit); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_niz_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_lava_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_S_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_ground_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_stop_ground_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_coof_ground_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_iter_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Object); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_exit); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_y_exit); \
		P_GET_UBOOL_REF(Z_Param_Out_S_en_exit); \
		P_GET_UBOOL_REF(Z_Param_Out_L_en_exit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerateLvl1_Ground(Z_Param_verh,Z_Param_niz,Z_Param_min_H,Z_Param_max_H,Z_Param_min_L,Z_Param_max_L,Z_Param_now_ground,Z_Param_stop_ground,Z_Param_now_lava,Z_Param_stop_lava,Z_Param_now_S,Z_Param_stop_S,Z_Param_coof_ground,Z_Param_iter,Z_Param_seed,Z_Param_z,Z_Param_S_en,Z_Param_L_en,Z_Param_Out_verh_exit,Z_Param_Out_niz_exit,Z_Param_Out_now_lava_exit,Z_Param_Out_now_S_exit,Z_Param_Out_now_ground_exit,Z_Param_Out_stop_ground_exit,Z_Param_Out_coof_ground_exit,Z_Param_Out_iter_exit,Z_Param_Out_Object,Z_Param_Out_z_exit,Z_Param_Out_y_exit,Z_Param_Out_S_en_exit,Z_Param_Out_L_en_exit); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerateSeed) \
	{ \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Seed); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerateSeed(Z_Param_Out_Seed); \
		P_NATIVE_END; \
	}


#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChooseChest) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Seed); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_out_chest); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::ChooseChest(Z_Param_Seed,Z_Param_Out_out_chest); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerStandart) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_z); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_coof_hard); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_block); \
		P_GET_PROPERTY(UIntProperty,Z_Param_cout_up_down); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_now_z_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_block_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_up_down_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_object); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerStandart(Z_Param_Seed,Z_Param_now_z,Z_Param_coof_hard,Z_Param_now_block,Z_Param_cout_up_down,Z_Param_Out_now_z_out,Z_Param_Out_now_block_out,Z_Param_Out_cout_up_down_out,Z_Param_Out_type_object); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerseparator) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_z); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_z_top); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_coof_hard); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_block); \
		P_GET_PROPERTY(UIntProperty,Z_Param_type_separator); \
		P_GET_PROPERTY(UIntProperty,Z_Param_cout_up_down_top); \
		P_GET_PROPERTY(UIntProperty,Z_Param_cout_up_down); \
		P_GET_PROPERTY(UIntProperty,Z_Param_expect); \
		P_GET_PROPERTY(UIntProperty,Z_Param_back_separator); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_top); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_down); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_block_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_separator_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_up_down_top_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_up_down_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_cout_downPlat); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ts_platform_top); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ts_platform_down); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_expect_out); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::Generseparator(Z_Param_seed,Z_Param_now_z,Z_Param_now_z_top,Z_Param_coof_hard,Z_Param_now_block,Z_Param_type_separator,Z_Param_cout_up_down_top,Z_Param_cout_up_down,Z_Param_expect,Z_Param_back_separator,Z_Param_Out_z_top,Z_Param_Out_z_down,Z_Param_Out_now_block_out,Z_Param_Out_type_separator_out,Z_Param_Out_cout_up_down_top_out,Z_Param_Out_cout_up_down_out,Z_Param_Out_cout_downPlat,Z_Param_Out_ts_platform_top,Z_Param_Out_ts_platform_down,Z_Param_Out_expect_out); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerAngel) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_segment); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_position); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_position_top); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_top_position); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_down_top); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_down_position); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_object_top); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_object_down); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_segment_out); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerAngel(Z_Param_now_segment,Z_Param_now_position,Z_Param_now_position_top,Z_Param_Out_top_position,Z_Param_Out_down_top,Z_Param_Out_down_position,Z_Param_Out_type_object_top,Z_Param_Out_type_object_down,Z_Param_Out_now_segment_out); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenBaseSituation) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_object); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_vert); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_block); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_part_object); \
		P_GET_PROPERTY(UIntProperty,Z_Param_len); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_pos_top); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_len_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_type_spawn_object); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_object_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_part_object_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_block_out); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_gener_down); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenBaseSituation(Z_Param_seed,Z_Param_now_object,Z_Param_now_vert,Z_Param_now_block,Z_Param_now_part_object,Z_Param_len,Z_Param_Out_pos_top,Z_Param_Out_len_out,Z_Param_Out_type_spawn_object,Z_Param_Out_now_object_out,Z_Param_Out_now_part_object_out,Z_Param_Out_now_block_out,Z_Param_Out_gener_down); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenMoneyLvl1) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_z); \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_chance); \
		P_GET_UBOOL_REF(Z_Param_Out_money); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_money); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenMoneyLvl1(Z_Param_z,Z_Param_seed,Z_Param_chance,Z_Param_Out_money,Z_Param_Out_z_money); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerateLvl1_Ground) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_verh); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_niz); \
		P_GET_PROPERTY(UIntProperty,Z_Param_min_H); \
		P_GET_PROPERTY(UIntProperty,Z_Param_max_H); \
		P_GET_PROPERTY(UIntProperty,Z_Param_min_L); \
		P_GET_PROPERTY(UIntProperty,Z_Param_max_L); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_ground); \
		P_GET_PROPERTY(UIntProperty,Z_Param_stop_ground); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_lava); \
		P_GET_PROPERTY(UIntProperty,Z_Param_stop_lava); \
		P_GET_PROPERTY(UIntProperty,Z_Param_now_S); \
		P_GET_PROPERTY(UIntProperty,Z_Param_stop_S); \
		P_GET_PROPERTY(UIntProperty,Z_Param_coof_ground); \
		P_GET_PROPERTY(UIntProperty,Z_Param_iter); \
		P_GET_PROPERTY(UIntProperty,Z_Param_seed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_z); \
		P_GET_UBOOL(Z_Param_S_en); \
		P_GET_UBOOL(Z_Param_L_en); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_verh_exit); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_niz_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_lava_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_S_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_now_ground_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_stop_ground_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_coof_ground_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_iter_exit); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Object); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_z_exit); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_y_exit); \
		P_GET_UBOOL_REF(Z_Param_Out_S_en_exit); \
		P_GET_UBOOL_REF(Z_Param_Out_L_en_exit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerateLvl1_Ground(Z_Param_verh,Z_Param_niz,Z_Param_min_H,Z_Param_max_H,Z_Param_min_L,Z_Param_max_L,Z_Param_now_ground,Z_Param_stop_ground,Z_Param_now_lava,Z_Param_stop_lava,Z_Param_now_S,Z_Param_stop_S,Z_Param_coof_ground,Z_Param_iter,Z_Param_seed,Z_Param_z,Z_Param_S_en,Z_Param_L_en,Z_Param_Out_verh_exit,Z_Param_Out_niz_exit,Z_Param_Out_now_lava_exit,Z_Param_Out_now_S_exit,Z_Param_Out_now_ground_exit,Z_Param_Out_stop_ground_exit,Z_Param_Out_coof_ground_exit,Z_Param_Out_iter_exit,Z_Param_Out_Object,Z_Param_Out_z_exit,Z_Param_Out_y_exit,Z_Param_Out_S_en_exit,Z_Param_Out_L_en_exit); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGenerateSeed) \
	{ \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Seed); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UGenerate_lvl1::GenerateSeed(Z_Param_Out_Seed); \
		P_NATIVE_END; \
	}


#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGenerate_lvl1(); \
	friend struct Z_Construct_UClass_UGenerate_lvl1_Statics; \
public: \
	DECLARE_CLASS(UGenerate_lvl1, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/The_Offer"), NO_API) \
	DECLARE_SERIALIZER(UGenerate_lvl1)


#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUGenerate_lvl1(); \
	friend struct Z_Construct_UClass_UGenerate_lvl1_Statics; \
public: \
	DECLARE_CLASS(UGenerate_lvl1, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/The_Offer"), NO_API) \
	DECLARE_SERIALIZER(UGenerate_lvl1)


#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGenerate_lvl1(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGenerate_lvl1) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGenerate_lvl1); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGenerate_lvl1); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGenerate_lvl1(UGenerate_lvl1&&); \
	NO_API UGenerate_lvl1(const UGenerate_lvl1&); \
public:


#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGenerate_lvl1(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGenerate_lvl1(UGenerate_lvl1&&); \
	NO_API UGenerate_lvl1(const UGenerate_lvl1&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGenerate_lvl1); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGenerate_lvl1); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGenerate_lvl1)


#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_PRIVATE_PROPERTY_OFFSET
#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_12_PROLOG
#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_PRIVATE_PROPERTY_OFFSET \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_SPARSE_DATA \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_RPC_WRAPPERS \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_INCLASS \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_PRIVATE_PROPERTY_OFFSET \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_SPARSE_DATA \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_INCLASS_NO_PURE_DECLS \
	The_Offer_Source_The_Offer_Public_Generate_lvl1_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> THE_OFFER_API UClass* StaticClass<class UGenerate_lvl1>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID The_Offer_Source_The_Offer_Public_Generate_lvl1_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
