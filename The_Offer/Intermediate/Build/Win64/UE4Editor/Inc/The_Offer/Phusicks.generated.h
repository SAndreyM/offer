// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef THE_OFFER_Phusicks_generated_h
#error "Phusicks.generated.h already included, missing '#pragma once' in Phusicks.h"
#endif
#define THE_OFFER_Phusicks_generated_h

#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_SPARSE_DATA
#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execJump_C) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_speed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_time); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_start_Z); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_Z); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_position_Z); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UPhusicks::Jump_C(Z_Param_speed,Z_Param_time,Z_Param_start_Z,Z_Param_now_Z,Z_Param_Out_position_Z); \
		P_NATIVE_END; \
	}


#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execJump_C) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_speed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_time); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_start_Z); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_now_Z); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_position_Z); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UPhusicks::Jump_C(Z_Param_speed,Z_Param_time,Z_Param_start_Z,Z_Param_now_Z,Z_Param_Out_position_Z); \
		P_NATIVE_END; \
	}


#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhusicks(); \
	friend struct Z_Construct_UClass_UPhusicks_Statics; \
public: \
	DECLARE_CLASS(UPhusicks, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/The_Offer"), NO_API) \
	DECLARE_SERIALIZER(UPhusicks)


#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPhusicks(); \
	friend struct Z_Construct_UClass_UPhusicks_Statics; \
public: \
	DECLARE_CLASS(UPhusicks, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/The_Offer"), NO_API) \
	DECLARE_SERIALIZER(UPhusicks)


#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhusicks(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhusicks) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhusicks); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhusicks); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhusicks(UPhusicks&&); \
	NO_API UPhusicks(const UPhusicks&); \
public:


#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhusicks(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhusicks(UPhusicks&&); \
	NO_API UPhusicks(const UPhusicks&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhusicks); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhusicks); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhusicks)


#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_PRIVATE_PROPERTY_OFFSET
#define The_Offer_Source_The_Offer_Public_Phusicks_h_12_PROLOG
#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_PRIVATE_PROPERTY_OFFSET \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_SPARSE_DATA \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_RPC_WRAPPERS \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_INCLASS \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define The_Offer_Source_The_Offer_Public_Phusicks_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_PRIVATE_PROPERTY_OFFSET \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_SPARSE_DATA \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_INCLASS_NO_PURE_DECLS \
	The_Offer_Source_The_Offer_Public_Phusicks_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> THE_OFFER_API UClass* StaticClass<class UPhusicks>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID The_Offer_Source_The_Offer_Public_Phusicks_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
