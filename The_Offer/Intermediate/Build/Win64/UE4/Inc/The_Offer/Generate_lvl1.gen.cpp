// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "The_Offer/Public/Generate_lvl1.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGenerate_lvl1() {}
// Cross Module References
	THE_OFFER_API UClass* Z_Construct_UClass_UGenerate_lvl1_NoRegister();
	THE_OFFER_API UClass* Z_Construct_UClass_UGenerate_lvl1();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_The_Offer();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_ChooseChest();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerAngel();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_Generseparator();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerStandart();
	THE_OFFER_API UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1();
// End Cross Module References
	void UGenerate_lvl1::StaticRegisterNativesUGenerate_lvl1()
	{
		UClass* Class = UGenerate_lvl1::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ChooseChest", &UGenerate_lvl1::execChooseChest },
			{ "GenBaseSituation", &UGenerate_lvl1::execGenBaseSituation },
			{ "GenerAngel", &UGenerate_lvl1::execGenerAngel },
			{ "GenerateLvl1_Ground", &UGenerate_lvl1::execGenerateLvl1_Ground },
			{ "GenerateSeed", &UGenerate_lvl1::execGenerateSeed },
			{ "Generseparator", &UGenerate_lvl1::execGenerseparator },
			{ "GenerStandart", &UGenerate_lvl1::execGenerStandart },
			{ "GenMoneyLvl1", &UGenerate_lvl1::execGenMoneyLvl1 },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics
	{
		struct Generate_lvl1_eventChooseChest_Parms
		{
			int32 Seed;
			int32 out_chest;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_out_chest;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Seed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventChooseChest_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventChooseChest_Parms), &Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_out_chest = { "out_chest", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventChooseChest_Parms, out_chest), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_Seed = { "Seed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventChooseChest_Parms, Seed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_out_chest,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::NewProp_Seed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_for_All" },
		{ "Keywords", "Choose_chest" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "ChooseChest", nullptr, nullptr, sizeof(Generate_lvl1_eventChooseChest_Parms), Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_ChooseChest()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_ChooseChest_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics
	{
		struct Generate_lvl1_eventGenBaseSituation_Parms
		{
			int32 seed;
			int32 now_object;
			float now_vert;
			int32 now_block;
			int32 now_part_object;
			int32 len;
			float pos_top;
			int32 len_out;
			int32 type_spawn_object;
			int32 now_object_out;
			int32 now_part_object_out;
			int32 now_block_out;
			int32 gener_down;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_gener_down;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_block_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_part_object_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_object_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_type_spawn_object;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_len_out;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_pos_top;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_len;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_part_object;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_block;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_vert;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_object;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_seed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenBaseSituation_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenBaseSituation_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_gener_down = { "gener_down", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, gener_down), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_block_out = { "now_block_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, now_block_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_part_object_out = { "now_part_object_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, now_part_object_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_object_out = { "now_object_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, now_object_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_type_spawn_object = { "type_spawn_object", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, type_spawn_object), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_len_out = { "len_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, len_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_pos_top = { "pos_top", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, pos_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_len = { "len", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, len), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_part_object = { "now_part_object", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, now_part_object), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_block = { "now_block", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, now_block), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_vert = { "now_vert", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, now_vert), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_object = { "now_object", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, now_object), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_seed = { "seed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenBaseSituation_Parms, seed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_gener_down,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_block_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_part_object_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_object_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_type_spawn_object,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_len_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_pos_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_len,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_part_object,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_block,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_vert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_now_object,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::NewProp_seed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_Lvl1" },
		{ "Comment", "/* \xd0\xa1\xd0\xba\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xba\xd0\xbe \xd0\xb3\xd0\xb5\xd0\xbd\xd0\xb5\xd1\x80\xd0\xb8\xd1\x80\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd1\x8c \xd0\xbf\xd0\xbb\xd0\xb0\xd1\x82\xd1\x84\xd0\xbe\xd1\x80\xd0\xbc \xd0\xb2\xd0\xbd\xd0\xb8\xd0\xb7 */" },
		{ "Keywords", "GenBaseSituation" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
		{ "ToolTip", "\xd0\xa1\xd0\xba\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xba\xd0\xbe \xd0\xb3\xd0\xb5\xd0\xbd\xd0\xb5\xd1\x80\xd0\xb8\xd1\x80\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd1\x8c \xd0\xbf\xd0\xbb\xd0\xb0\xd1\x82\xd1\x84\xd0\xbe\xd1\x80\xd0\xbc \xd0\xb2\xd0\xbd\xd0\xb8\xd0\xb7" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "GenBaseSituation", nullptr, nullptr, sizeof(Generate_lvl1_eventGenBaseSituation_Parms), Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics
	{
		struct Generate_lvl1_eventGenerAngel_Parms
		{
			int32 now_segment;
			float now_position;
			float now_position_top;
			float top_position;
			int32 down_top;
			float down_position;
			int32 type_object_top;
			int32 type_object_down;
			int32 now_segment_out;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_segment_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_type_object_down;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_type_object_top;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_down_position;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_down_top;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_top_position;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_position_top;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_position;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_segment;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerAngel_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerAngel_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_segment_out = { "now_segment_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, now_segment_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_type_object_down = { "type_object_down", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, type_object_down), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_type_object_top = { "type_object_top", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, type_object_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_down_position = { "down_position", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, down_position), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_down_top = { "down_top", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, down_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_top_position = { "top_position", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, top_position), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_position_top = { "now_position_top", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, now_position_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_position = { "now_position", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, now_position), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_segment = { "now_segment", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerAngel_Parms, now_segment), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_segment_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_type_object_down,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_type_object_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_down_position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_down_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_top_position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_position_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::NewProp_now_segment,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_Lvl1" },
		{ "Comment", "/* Now fragment situation \"Angel\" x/25 */" },
		{ "Keywords", "GenerAngel" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
		{ "ToolTip", "Now fragment situation \"Angel\" x/25" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "GenerAngel", nullptr, nullptr, sizeof(Generate_lvl1_eventGenerAngel_Parms), Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerAngel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_GenerAngel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics
	{
		struct Generate_lvl1_eventGenerateLvl1_Ground_Parms
		{
			float verh;
			float niz;
			int32 min_H;
			int32 max_H;
			int32 min_L;
			int32 max_L;
			int32 now_ground;
			int32 stop_ground;
			int32 now_lava;
			int32 stop_lava;
			int32 now_S;
			int32 stop_S;
			int32 coof_ground;
			int32 iter;
			int32 seed;
			float z;
			bool S_en;
			bool L_en;
			float verh_exit;
			float niz_exit;
			int32 now_lava_exit;
			int32 now_S_exit;
			int32 now_ground_exit;
			int32 stop_ground_exit;
			int32 coof_ground_exit;
			int32 iter_exit;
			int32 Object;
			float z_exit;
			float y_exit;
			bool S_en_exit;
			bool L_en_exit;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static void NewProp_L_en_exit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_L_en_exit;
		static void NewProp_S_en_exit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_S_en_exit;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_y_exit;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_z_exit;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Object;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_iter_exit;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_coof_ground_exit;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_stop_ground_exit;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_ground_exit;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_S_exit;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_lava_exit;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_niz_exit;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_verh_exit;
		static void NewProp_L_en_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_L_en;
		static void NewProp_S_en_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_S_en;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_z;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_seed;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_iter;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_coof_ground;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_stop_S;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_S;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_stop_lava;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_lava;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_stop_ground;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_ground;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_max_L;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_min_L;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_max_H;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_min_H;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_niz;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_verh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerateLvl1_Ground_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerateLvl1_Ground_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en_exit_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerateLvl1_Ground_Parms*)Obj)->L_en_exit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en_exit = { "L_en_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerateLvl1_Ground_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en_exit_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en_exit_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerateLvl1_Ground_Parms*)Obj)->S_en_exit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en_exit = { "S_en_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerateLvl1_Ground_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en_exit_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_y_exit = { "y_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, y_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_z_exit = { "z_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, z_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_Object = { "Object", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, Object), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_iter_exit = { "iter_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, iter_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_coof_ground_exit = { "coof_ground_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, coof_ground_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_ground_exit = { "stop_ground_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, stop_ground_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_ground_exit = { "now_ground_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, now_ground_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_S_exit = { "now_S_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, now_S_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_lava_exit = { "now_lava_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, now_lava_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_niz_exit = { "niz_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, niz_exit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_verh_exit = { "verh_exit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, verh_exit), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerateLvl1_Ground_Parms*)Obj)->L_en = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en = { "L_en", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerateLvl1_Ground_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerateLvl1_Ground_Parms*)Obj)->S_en = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en = { "S_en", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerateLvl1_Ground_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_z = { "z", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, z), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_seed = { "seed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, seed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_iter = { "iter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, iter), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_coof_ground = { "coof_ground", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, coof_ground), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_S = { "stop_S", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, stop_S), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_S = { "now_S", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, now_S), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_lava = { "stop_lava", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, stop_lava), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_lava = { "now_lava", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, now_lava), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_ground = { "stop_ground", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, stop_ground), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_ground = { "now_ground", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, now_ground), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_max_L = { "max_L", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, max_L), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_min_L = { "min_L", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, min_L), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_max_H = { "max_H", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, max_H), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_min_H = { "min_H", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, min_H), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_niz = { "niz", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, niz), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_verh = { "verh", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateLvl1_Ground_Parms, verh), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_y_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_z_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_Object,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_iter_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_coof_ground_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_ground_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_ground_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_S_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_lava_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_niz_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_verh_exit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_L_en,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_S_en,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_seed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_iter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_coof_ground,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_S,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_S,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_lava,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_lava,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_stop_ground,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_now_ground,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_max_L,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_min_L,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_max_H,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_min_H,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_niz,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::NewProp_verh,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_Lvl1" },
		{ "Comment", "/* \xd0\x90 \xd1\x81\xd0\xb5\xd0\xb9\xd1\x87\xd0\xb0\xd1\x81 \xd1\x82\xd0\xbe\xd0\xb6\xd0\xb5 \xd1\x81\xd0\xb0\xd0\xbc\xd0\xbe\xd0\xb5 \xd1\x82\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xba\xd0\xbe \xd1\x81 \xd0\xbf\xd0\xbe\xd1\x81\xd1\x82\xd1\x84\xd0\xb8\xd0\xba\xd1\x81\xd0\xbe\xd0\xbc \"_exit\"...*/" },
		{ "Kewords", "GenerateLvl1_ground" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
		{ "ToolTip", "\xd0\x90 \xd1\x81\xd0\xb5\xd0\xb9\xd1\x87\xd0\xb0\xd1\x81 \xd1\x82\xd0\xbe\xd0\xb6\xd0\xb5 \xd1\x81\xd0\xb0\xd0\xbc\xd0\xbe\xd0\xb5 \xd1\x82\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xba\xd0\xbe \xd1\x81 \xd0\xbf\xd0\xbe\xd1\x81\xd1\x82\xd1\x84\xd0\xb8\xd0\xba\xd1\x81\xd0\xbe\xd0\xbc \"_exit\"..." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "GenerateLvl1_Ground", nullptr, nullptr, sizeof(Generate_lvl1_eventGenerateLvl1_Ground_Parms), Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics
	{
		struct Generate_lvl1_eventGenerateSeed_Parms
		{
			int32 Seed;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Seed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerateSeed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerateSeed_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::NewProp_Seed = { "Seed", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerateSeed_Parms, Seed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::NewProp_Seed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_for_All" },
		{ "Kewords", "GenerateSeed" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "GenerateSeed", nullptr, nullptr, sizeof(Generate_lvl1_eventGenerateSeed_Parms), Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics
	{
		struct Generate_lvl1_eventGenerseparator_Parms
		{
			int32 seed;
			float now_z;
			float now_z_top;
			float coof_hard;
			int32 now_block;
			int32 type_separator;
			int32 cout_up_down_top;
			int32 cout_up_down;
			int32 expect;
			int32 back_separator;
			float z_top;
			float z_down;
			int32 now_block_out;
			int32 type_separator_out;
			int32 cout_up_down_top_out;
			int32 cout_up_down_out;
			int32 cout_downPlat;
			int32 ts_platform_top;
			int32 ts_platform_down;
			int32 expect_out;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_expect_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ts_platform_down;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ts_platform_top;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_cout_downPlat;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_cout_up_down_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_cout_up_down_top_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_type_separator_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_block_out;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_z_down;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_z_top;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_back_separator;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_expect;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_cout_up_down;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_cout_up_down_top;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_type_separator;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_block;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_coof_hard;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_z_top;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_z;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_seed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerseparator_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerseparator_Parms), &Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_expect_out = { "expect_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, expect_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ts_platform_down = { "ts_platform_down", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, ts_platform_down), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ts_platform_top = { "ts_platform_top", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, ts_platform_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_downPlat = { "cout_downPlat", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, cout_downPlat), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down_out = { "cout_up_down_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, cout_up_down_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down_top_out = { "cout_up_down_top_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, cout_up_down_top_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_type_separator_out = { "type_separator_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, type_separator_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_block_out = { "now_block_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, now_block_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_z_down = { "z_down", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, z_down), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_z_top = { "z_top", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, z_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_back_separator = { "back_separator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, back_separator), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_expect = { "expect", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, expect), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down = { "cout_up_down", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, cout_up_down), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down_top = { "cout_up_down_top", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, cout_up_down_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_type_separator = { "type_separator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, type_separator), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_block = { "now_block", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, now_block), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_coof_hard = { "coof_hard", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, coof_hard), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_z_top = { "now_z_top", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, now_z_top), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_z = { "now_z", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, now_z), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_seed = { "seed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerseparator_Parms, seed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_expect_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ts_platform_down,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_ts_platform_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_downPlat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down_top_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_type_separator_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_block_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_z_down,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_z_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_back_separator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_expect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_cout_up_down_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_type_separator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_block,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_coof_hard,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_z_top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_now_z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::NewProp_seed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_Lvl1" },
		{ "Comment", "/* Variable for storage value for coefficient changes z-coordinates */" },
		{ "Keywords", "Generseparator" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
		{ "ToolTip", "Variable for storage value for coefficient changes z-coordinates" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "Generseparator", nullptr, nullptr, sizeof(Generate_lvl1_eventGenerseparator_Parms), Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_Generseparator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_Generseparator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics
	{
		struct Generate_lvl1_eventGenerStandart_Parms
		{
			int32 Seed;
			float now_z;
			float coof_hard;
			int32 now_block;
			int32 cout_up_down;
			float now_z_out;
			int32 now_block_out;
			int32 cout_up_down_out;
			int32 type_object;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_type_object;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_cout_up_down_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_block_out;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_z_out;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_cout_up_down;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_now_block;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_coof_hard;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_now_z;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Seed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenerStandart_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenerStandart_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_type_object = { "type_object", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, type_object), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_cout_up_down_out = { "cout_up_down_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, cout_up_down_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_block_out = { "now_block_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, now_block_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_z_out = { "now_z_out", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, now_z_out), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_cout_up_down = { "cout_up_down", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, cout_up_down), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_block = { "now_block", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, now_block), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_coof_hard = { "coof_hard", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, coof_hard), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_z = { "now_z", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, now_z), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_Seed = { "Seed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenerStandart_Parms, Seed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_type_object,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_cout_up_down_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_block_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_z_out,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_cout_up_down,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_block,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_coof_hard,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_now_z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::NewProp_Seed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_Lvl1" },
		{ "Keywords", "GenerStandart" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "GenerStandart", nullptr, nullptr, sizeof(Generate_lvl1_eventGenerStandart_Parms), Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenerStandart()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_GenerStandart_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics
	{
		struct Generate_lvl1_eventGenMoneyLvl1_Parms
		{
			float z;
			int32 seed;
			float chance;
			bool money;
			float z_money;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_z_money;
		static void NewProp_money_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_money;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_chance;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_seed;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_z;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenMoneyLvl1_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenMoneyLvl1_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_z_money = { "z_money", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenMoneyLvl1_Parms, z_money), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_money_SetBit(void* Obj)
	{
		((Generate_lvl1_eventGenMoneyLvl1_Parms*)Obj)->money = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_money = { "money", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Generate_lvl1_eventGenMoneyLvl1_Parms), &Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_money_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_chance = { "chance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenMoneyLvl1_Parms, chance), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_seed = { "seed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenMoneyLvl1_Parms, seed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_z = { "z", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Generate_lvl1_eventGenMoneyLvl1_Parms, z), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_z_money,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_money,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_chance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_seed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::NewProp_z,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::Function_MetaDataParams[] = {
		{ "Category", "Generate_Lvl1" },
		{ "Keywords", "GenMoneyLvl1" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGenerate_lvl1, nullptr, "GenMoneyLvl1", nullptr, nullptr, sizeof(Generate_lvl1_eventGenMoneyLvl1_Parms), Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGenerate_lvl1_NoRegister()
	{
		return UGenerate_lvl1::StaticClass();
	}
	struct Z_Construct_UClass_UGenerate_lvl1_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGenerate_lvl1_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_The_Offer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGenerate_lvl1_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGenerate_lvl1_ChooseChest, "ChooseChest" }, // 2242142028
		{ &Z_Construct_UFunction_UGenerate_lvl1_GenBaseSituation, "GenBaseSituation" }, // 3500822187
		{ &Z_Construct_UFunction_UGenerate_lvl1_GenerAngel, "GenerAngel" }, // 1037455679
		{ &Z_Construct_UFunction_UGenerate_lvl1_GenerateLvl1_Ground, "GenerateLvl1_Ground" }, // 4225446350
		{ &Z_Construct_UFunction_UGenerate_lvl1_GenerateSeed, "GenerateSeed" }, // 967094578
		{ &Z_Construct_UFunction_UGenerate_lvl1_Generseparator, "Generseparator" }, // 2916461673
		{ &Z_Construct_UFunction_UGenerate_lvl1_GenerStandart, "GenerStandart" }, // 66243635
		{ &Z_Construct_UFunction_UGenerate_lvl1_GenMoneyLvl1, "GenMoneyLvl1" }, // 593498237
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGenerate_lvl1_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Generate_lvl1.h" },
		{ "ModuleRelativePath", "Public/Generate_lvl1.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGenerate_lvl1_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGenerate_lvl1>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGenerate_lvl1_Statics::ClassParams = {
		&UGenerate_lvl1::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGenerate_lvl1_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGenerate_lvl1_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGenerate_lvl1()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGenerate_lvl1_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGenerate_lvl1, 2661030467);
	template<> THE_OFFER_API UClass* StaticClass<UGenerate_lvl1>()
	{
		return UGenerate_lvl1::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGenerate_lvl1(Z_Construct_UClass_UGenerate_lvl1, &UGenerate_lvl1::StaticClass, TEXT("/Script/The_Offer"), TEXT("UGenerate_lvl1"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGenerate_lvl1);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
