// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Generate_lvl1.generated.h"

/**
 * 
 */
UCLASS()
class THE_OFFER_API UGenerate_lvl1 : public UBlueprintFunctionLibrary
{
	GENERATED_BODY() public:
		UFUNCTION(BlueprintCallable, Category = "Generate_for_All", meta = (Kewords = "GenerateSeed"))
		static bool GenerateSeed(int& Seed);
		UFUNCTION(BlueprintCallable, Category = "Generate_Lvl1", meta = (Kewords = "GenerateLvl1_ground"))
		static bool GenerateLvl1_Ground(float verh, float niz, /*  эти переменные кароч ограниители спавна платформ ниже-выше */
                            int min_H, int max_H, /*переменные-границы спавна шипов */
                            int min_L, int max_L, /*переменные-границы спавна лавы */
                            int now_ground, int stop_ground, /*1) текущее кол-во прфмых платформ подряд 2) ограничение на кол-во платформ подряд*/
                            int now_lava, int stop_lava, /*тоже самое что и выше только для лавы*/
                            int now_S, int stop_S, /* тоже самое что и выше только для шипов*/
                            int coof_ground, /* коэффициент для изменения ограничения кол-ва платформ подряд*/
                            int iter, int seed, /* текущая иттерация и сид*/
                            float z, /* текущая позиция спавна плоков по высоте*/
                            bool S_en, bool L_en, /* просто блокируем спавн связки лава-потом шипы*/
                            /* А сейчас тоже самое только с постфиксом "_exit"...*/
                            float& verh_exit, float& niz_exit,
                            int& now_lava_exit,
                            int& now_S_exit,
                            int& now_ground_exit, int& stop_ground_exit, 
                            int& coof_ground_exit, 
                            int& iter_exit, 
                            int& Object,
                            float& z_exit, float& y_exit,
                            bool& S_en_exit, bool& L_en_exit);
        UFUNCTION(BlueprintCallable, Category = "Generate_Lvl1", meta = (Keywords = "GenMoneyLvl1"))
        static bool GenMoneyLvl1(float z, int seed, float chance, bool& money, float& z_money);
        UFUNCTION(BlueprintCallable, Category = "Generate_Lvl1", meta = (Keywords = "GenBaseSituation"))
        static bool GenBaseSituation(int seed, /* Сид, ну тут ясно */
                                int now_object, /* Текущий объект ситуации */
                                float now_vert, /* Вертикальная координата (это так: |, а не так: ---)*/
                                int now_block, /* Текущий генерируемы блок по счёту */
                                int now_part_object, /* текущий генерируемый блок объекта */
                                int len, /* длина объекта */
                                float& pos_top, /* Генерируем по этой координате */
                                int& len_out, /* длина объекта */
                                int& type_spawn_object, /* Тиg объекта для спавна, тут всё ясно */
                                int& now_object_out, /* Текущий объект ситуации */
                                int& now_part_object_out, /* текущий генерируемый блок объекта */
                                int& now_block_out, /* Текущий генерируемы блок по счёту */
                                int& gener_down /* Сколько генерировать платформ вниз */);
        UFUNCTION(BlueprintCallable, Category = "Generate_Lvl1", meta = (Keywords = "GenerAngel"))
        static bool GenerAngel(int now_segment, /* Now fragment situation "Angel" x/25 */
                            float now_position, /* Now position Z-coordinates */
                            float now_position_top, /* Now position Z-coordinates for top platform */
                            float& top_position, /* Position for spawn top platform */
                            int& down_top, /* platform under top platform */
                            float& down_position, /* Position for spawn down platform */
                            int& type_object_top, /* Type object for spawn top platform */
                            int& type_object_down, /* Type object for spawn down platform */
                            int& now_segment_out    /* Now fragment situation "Angel" x/25 */
                            );
        UFUNCTION(BlueprintCallable, Category = "Generate_Lvl1", meta = (Keywords = "Generseparator"))
        static bool Generseparator(int seed,   /* Seed for generation */
                                
                                /*           FLOAT VARIABLES               */
                                float now_z,    /* Now vertical coordinates */
                                float now_z_top,    /* Now vertical coordinates top platform */
                                float coof_hard,    /* Cooficient hard-map*/
                                
                                /*          INTEGER VARIABLES              */

                                int now_block,  /* Now block this elemet */
                                int type_separator, /* Type separator (top - hard) or (down - hard) */
                                int cout_up_down_top,   /* Counter up or down spawn for top */
                                int cout_up_down,   /* Counter up or down spawn for down */
                                int expect, /* Variable for storage value for coefficient changes z-coordinates */
                                int back_separator,
                                
                                /*           FLOAT OUT VARIABLES            */

                                float& z_top,   /* Coordinates for spawn top_platform */   
                                float& z_down,  /* Coordinates for spawn down_platform */
                                
                                /*           INTEGER OUT VARIABLES          */
                                
                                int& now_block_out, /* Now block this element */
                                int& type_separator_out,    /* Type separator (top - hard) or (down - hard) */
                                int& cout_up_down_top_out,  /* Counter up or down spawn for top */
                                int& cout_up_down_out,  /* Counter up or down spawn for down */
                                int& cout_downPlat,  /* Counter platform under top_platform */
                                int& ts_platform_top,    /* Type spawn top platform */
                                int& ts_platform_down,   /* Type spawn down platform */
                                int& expect_out /* Variable for storage value for coefficient changes z-coordinates */
                                
                                );
        UFUNCTION(BlueprintCallable, Category = "Generate_Lvl1", meta = (Keywords = "GenerStandart"))
        static bool GenerStandart(int Seed,                                 
                                float now_z,    
                                float coof_hard,
                                int now_block,  
                                int cout_up_down,
                                float& now_z_out,
                                int& now_block_out,
                                int& cout_up_down_out,
                                int& type_object);
        UFUNCTION(BlueprintCallable, Category = "Generate_for_All", meta = (Keywords = "Choose_chest"))
        static bool ChooseChest(int Seed,
                                 int& out_chest);
};
