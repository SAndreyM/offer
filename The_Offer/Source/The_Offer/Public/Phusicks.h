// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Phusicks.generated.h"

/**
 * 
 */
UCLASS()
class THE_OFFER_API UPhusicks : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	public:
	UFUNCTION(BlueprintCallable, Category = "Custom_Phusicks", meta = (Kewords = "Jump_C"))
	static bool Jump_C(float speed, float time, float start_Z, float now_Z, float& position_Z);
};
