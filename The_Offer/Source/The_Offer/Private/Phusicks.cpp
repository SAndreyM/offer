// Fill out your copyright notice in the Description page of Project Settings.


#include "Phusicks.h"

bool UPhusicks::Jump_C(float speed, float time, float start_Z, float now_Z, float& position_Z)
{
    float delta_speed = 0;
    int g = 10;
    delta_speed = speed - g*time;
    position_Z = (start_Z + (speed * time - (g * time * time) / 2)) - now_Z;
    return true;
}