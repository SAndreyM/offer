// Fill out your copyright notice in the Description page of Project Settings.


#include "Generate_lvl1.h"
#include <random>
#include <vector>
using namespace std;
bool UGenerate_lvl1::GenerateSeed(int& Seed)
{
	random_device rd;
    mt19937 mersenne(rd());
    Seed = mersenne();
    return true;
}

bool UGenerate_lvl1::GenerateLvl1_Ground(float verh, float niz, /*  эти переменные кароч ограничители спавна платформ ниже-выше */
                            int min_H, int max_H, /*переменные-границы спавна шипов */
                            int min_L, int max_L, /*переменные-границы спавна лавы */
                            int now_ground, int stop_ground, /*1) текущее кол-во прфмых платформ подряд 2) ограничение на кол-во платформ подряд*/
                            int now_lava, int stop_lava, /*тоже самое что и выше только для лавы*/
                            int now_S, int stop_S, /* тоже самое что и выше только для шипов*/
                            int coof_ground, /* коэффициент для изменения ограничения кол-ва платформ подряд*/
                            int iter, int seed, /* текущая иттерация и сид*/
                            float z, /* текущая позиция спавна плоков по высоте*/
                            bool S_en, bool L_en, /* просто блокируем спавн связки лава-потом шипы*/
                            /* А сейчас тоже самое только с постфиксом "_exit"...*/
                            float& verh_exit, float& niz_exit,
                            int& now_lava_exit,
                            int& now_S_exit,
                            int& now_ground_exit, int& stop_ground_exit, 
                            int& coof_ground_exit, 
                            int& iter_exit, 
                            int& Object,
                            float& z_exit, float& y_exit,
                            bool& S_en_exit, bool& L_en_exit)
{
    float pos_pl = 80.0;
    int switch_coof = -1;
    mt19937 mersenne(seed);
    int generate = mersenne() % 1000;
    if (generate > verh && now_ground > 2)
    {
        switch_coof = 0;
    }
    if ((generate < niz && now_ground > 2) && switch_coof == -1)
    {
        switch_coof = 1;
    }
    if (((generate > min_H && generate < max_H && now_ground > 0 && S_en == true && now_S == 0) || (generate > min_H && generate < max_H && S_en == true && now_S < stop_S)) && switch_coof == -1)
    {
        switch_coof = 2;
    }

    
    if (((generate > min_L && generate < max_L && now_ground > 0 && L_en == true && now_lava == 0) || (generate > min_L && generate < max_L && L_en == true && now_lava < stop_lava)) && switch_coof == -1)
    {
        switch_coof = 3;
    }
    if ((now_ground < stop_ground) && switch_coof == -1)
    {
        switch_coof = 4;
    }
    if (switch_coof == -1)
    {
        return false;
    }
    else
    {
        switch (switch_coof)
        {
            case 0:
                y_exit = iter * pos_pl;
                z_exit = z + pos_pl;
                verh_exit += 50;
                niz_exit += 30;
                now_ground_exit = 0;
                Object = 0;
                S_en_exit = true;
                L_en_exit = true;
                coof_ground_exit = coof_ground;
                stop_ground_exit = stop_ground;
                now_S_exit = 0;
                now_lava_exit = 0;
                break;
            case 1:
                y_exit = iter * pos_pl;
                z_exit = z - pos_pl;
                verh_exit -= 50;
                niz_exit -= 30;
                now_ground_exit = 0;
                Object = 0;
                S_en_exit = true;
                L_en_exit = true;
                coof_ground_exit = coof_ground;
                stop_ground_exit = stop_ground;
                now_S_exit = 0;
                now_lava_exit = 0;
                break;
            case 2:
                y_exit = iter * pos_pl;
                z_exit = z;
                verh_exit = verh;
                niz_exit = niz;
                now_ground_exit = 0;
                Object = 1;
                S_en_exit = true;
                L_en_exit = false;
                coof_ground_exit = coof_ground;
                stop_ground_exit = stop_ground;
                now_S_exit = now_S + 1;
                now_lava_exit = 0;
                break;
            case 3:
                y_exit = iter * pos_pl;
                z_exit = z;
                verh_exit = verh;
                niz_exit = niz;
                now_ground_exit = 0;
                Object = 2;
                S_en_exit = false;
                L_en_exit = true;
                coof_ground_exit = coof_ground;
                stop_ground_exit = stop_ground;
                now_S_exit = 0;
                now_lava_exit = now_lava + 1;
                break;
            case 4:
                y_exit = iter * pos_pl;
                z_exit = z;
                verh_exit = verh - 3;
                niz_exit = niz + 3;
                now_ground_exit = now_ground + 1;
                Object = 0;
                S_en_exit = true;
                L_en_exit = true;
                coof_ground_exit = coof_ground;
                stop_ground_exit = stop_ground;
                now_S_exit = 0;
                now_lava_exit = 0;
                break;
        }
        if (2000 * coof_ground == iter && stop_ground > 3)
        {
            stop_ground_exit = stop_ground + 1;
            coof_ground_exit = coof_ground + 1;
        }
        iter_exit = iter + 1;
        return true;
    }

}


bool UGenerate_lvl1::GenMoneyLvl1(float z, /* Текущая координата спавна */
                              int seed, /* Сид генерации */
                              float chance, /* шанс спавна монетки */
                              bool& money, /* спавнить монетку или нет, вот в чём вопрос */
                              float& z_money /* где эта чёртова монетка */)
{
    int switch_coof = -1;
    float pos_pl = 80.0;
    mt19937 mersenne(seed);
    int generate = mersenne() % 100;
    int height = 0;
    if (generate < chance)
    {
        money = true;
        height = mersenne() % 3;
        if (height == 0)
        {
            z_money = z + 2 * pos_pl;
        }
        else
        {
            z_money = z + 3 * pos_pl;
        }
        
    }
    else
    {
        money = false;
    }
    return true;
}

bool UGenerate_lvl1::GenBaseSituation(int seed, /* Сид, ну тут ясно */
                                int now_object, /* Текущий объект ситуации */
                                float now_vert, /* Вертикальная координата (это так: |, а не так: ---)*/
                                int now_block, /* Текущий генерируемы блок по счёту */
                                int now_part_object, /* текущий генерируемый блок объекта */
                                int len, /* длина объекта */
                                float& pos_top, /* Генерируем по этой координате */
                                int& len_out, /* длина объекта */
                                int& type_spawn_object, /* Тиg объекта для спавна, тут всё ясно */
                                int& now_object_out, /* Текущий объект ситуации */
                                int& now_part_object_out, /* текущий генерируемый блок объекта */
                                int& now_block_out, /* Текущий генерируемы блок по счёту */
                                int& gener_down /* Сколько генерировать платформ вниз */)
{
    
    float pos_pl = 80.0;
    int rands = 0;
    int position = 0;
    bool spawn = true;
    int generate = 0;
    mt19937 mersenne(seed);
    if (now_object == -1)
    {
        generate = 0;
        generate = mersenne() % 100;
        if (generate < 40)
        {
            generate = 4;
        }
        else
        {
            if(generate >= 40 && generate < 55)
            {
                generate = 1;
            }
            else
            {
                if(generate >= 55 && generate < 70)
                {
                    generate = 2;
                }
                else
                {
                    if(generate >= 70 && generate < 85)
                    {
                        generate = 3;
                    }
                    else
                    {
                        if(generate >= 85 && generate < 100)
                        {
                            generate = 0;
                        }
                    }
                    
                }
                
            }
            
        }
        switch (generate)
        {
            case 0:
                now_object_out = 0;
                now_part_object_out = 0;
                now_object = 0;
                len_out = 9;
                len = 9;
                break;
            case 1:
                now_object_out = 1;
                now_part_object_out = 0;
                now_object = 1;
                len_out = 21;
                len = 21;
                break;
            case 2:
                now_object_out = 2;
                now_part_object_out = 0;
                now_object = 2;
                len_out = 27;
                len = 27;
                break;
            case 3:
                now_object_out = 3;
                now_part_object_out = 0;
                now_object = 3;
                len_out = 22;
                len = 22;
                break;
            case 4:
                now_object_out = 4;
                now_part_object_out = 0;
                now_object = 4;
                break;
        }
    }
    if (now_object != 4)
    {
        if (now_part_object < len)
        {
            spawn = true;
        }
        else
        {
            spawn = false;
        }

        if (spawn == true)
        {
            vector<float> bloc(3);
            switch (now_object)
            {
                case 0:
                    switch (now_part_object)
                    {
                        case 0:
                            bloc = {2, 2, 0};
                            break;
                        case 1:
                            bloc = {0, 4, 0};
                            break;
                        case 2:
                            bloc = {0, -1, 0};
                            break;
                        case 3:
                            bloc = {0, -1, 0};
                            break;
                        case 4:
                            bloc = {0, -1, 4};
                            break;
                        case 5:
                            bloc = {-1, -1, 4};
                            break;
                        case 6:
                            bloc = {-1, -1, 4};
                            break;
                        case 7:
                            bloc = {-1, -1, 0};
                            break;
                        case 8:
                            bloc = {0, 3, 0};
                            break;
                        case 9:
                            bloc = {0, 2, 0};
                            break;
                    }
                    break;
                case 1:
                    switch (now_part_object)
                    {
                        case 0:
                            bloc = {0, -1, 0};
                            break;
                        case 1:
                            bloc = {1, -1, 3};
                            break;
                        case 2:
                            bloc = {1, -1, 3};
                            break;
                        case 3:
                            bloc = {1, -1, 3};
                            break;
                        case 4:
                            bloc = {1, -1, 3};
                            break;
                        case 5:
                            bloc = {0, -1, 0};
                            break;
                        case 6:
                            bloc = {-2, -1, 0};
                            break;
                        case 7:
                            bloc = {1, -1, 0};
                            break;
                        case 8:
                            bloc = {1, -1, 0};
                            break;
                        case 9:
                            bloc = {-1, -1, 0};
                            break;
                        case 10:
                            bloc = {0, -1, 0};
                            break;
                        case 11:
                            bloc = {0, -1, 0};
                            break;
                        case 12:
                            bloc = {0, -1, 0};
                            break;
                        case 13:
                            bloc = {0, -1, 0};
                            break;
                        case 14:
                            bloc = {0, -1, 0};
                            break;
                        case 15:
                            bloc = {0, -1, 0};
                            break;
                        case 16:
                            bloc = {0, -1, 0};
                            break;
                        case 17:
                            bloc = {0, -1, 0};
                            break;
                        case 18:
                            bloc = {0, -1, 0};
                            break;
                        case 19:
                            bloc = {0, -1, 0};
                            break;
                        case 20:
                            bloc = {0, -1, 0};
                            break;
                    }
                    break;
                case 2:
                    switch (now_part_object)
                    {
                        case 0:
                            bloc = {0, 3, 0};
                            break;
                        case 1:
                            bloc = {-2, -1, 0};
                            break;
                        case 2:
                            bloc = {-1, -1, 0};
                            break;
                        case 3:
                            bloc = {0, 0, 0};
                            break;
                        case 4:
                            bloc = {1, 1, 0};
                            break;
                        case 5:
                            bloc = {1, 2, 0};
                            break;
                        case 6:
                            bloc = {-2, -1, 0};
                            break;
                        case 7:
                            bloc = {0, -1, 0};
                            break;
                        case 8:
                            bloc = {2, 2, 0};
                            break;
                        case 9:
                            bloc = {0, 0, 0};
                            break;
                        case 10:
                            bloc = {0, 2, 0};
                            break;
                        case 11:
                            bloc = {-2, -1, 0};
                            break;
                        case 12:
                            bloc = {0, -1, 0};
                            break;
                        case 13:
                            bloc = {2, 2, 0};
                            break;
                        case 14:
                            bloc = {1, 2, 3};
                            break;
                        case 15:
                            bloc = {0, 1, 0};
                            break;
                        case 16:
                            bloc = {0, 0, 0};
                            break;
                        case 17:
                            bloc = {0, 1, 0};
                            break;
                        case 18:
                            bloc = {0, -1, 0};
                            break;
                        case 19:
                            bloc = {0, -1, 0};
                            break;
                        case 20:
                            bloc = {0, 1, 0};
                            break;
                        case 21:
                            bloc = {0, 0, 0};
                            break;
                        case 22:
                            bloc = {0, 0, 0};
                            break;
                        case 23:
                            bloc = {0, 1, 0};
                            break;
                        case 24:
                            bloc = {0, -1, 0};
                            break;
                        case 25:
                            bloc = {0, -1, 0};
                            break;
                        case 26:
                            bloc = {0, 1, 0};
                            break;
                    }
                    break;
                case 3:
                    switch (now_part_object)
                    {
                        case 0:
                            bloc = {0, 1, 0};
                            break;
                        case 1:
                            bloc = {0, -1, 0};
                            break;
                        case 2:
                            bloc = {0, 1, 0};
                            break;
                        case 3:
                            bloc = {1, 1, 3};
                            break;
                        case 4:
                            bloc = {1, 1, 3};
                            break;
                        case 5:
                            bloc = {0, 0, 0};
                            break;
                        case 6:
                            bloc = {0, 0, 0};
                            break;
                        case 7:
                            bloc = {0, -1, 0};
                            break;
                        case 8:
                            bloc = {0, 0, 0};
                            break;
                        case 9:
                            bloc = {0, 0, 0};
                            break;
                        case 10:
                            bloc = {2, 2, 0};
                            break;
                        case 11:
                            bloc = {0, 2, 0};
                            break;
                        case 12:
                            bloc = {0, 2, 0};
                            break;
                        case 13:
                            bloc = {-2, 1, 0};
                            break;
                        case 14:
                            bloc = {0, -1, 0};
                            break;
                        case 15:
                            bloc = {0, 1, 0};
                            break;
                        case 16:
                            bloc = {2, 2, 0};
                            break;
                        case 17:
                            bloc = {0, 2, 0};
                            break;
                        case 18:
                            bloc = {0, 3, 0};
                            break;
                        case 19:
                            bloc = {0, -1, 0};
                            break;
                        case 20:
                            bloc = {-1, 2, 0};
                            break;
                        case 21:
                            bloc = {-1, 0, 0};
                            break;
                    }
                    break;
            }
            pos_top = now_vert + bloc[0] * pos_pl;
            gener_down = bloc[1];
            type_spawn_object = bloc[2];
            now_part_object_out = now_part_object + 1;
            now_block_out = now_block + 1;
            len_out = len;
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        if(now_part_object == 0)
        {
            generate = mersenne() % 12;
            if (generate < 3 && generate < 0)
            {
                generate = 3;
            }
            else
            {
                if (generate == 0)
                {
                    generate = 12;
                }
                
            }
            len = generate;
            if (now_vert > 0)
            {                
                rands = mersenne() % 2;
                if (rands != 0)
                {
                    position = rands;
                }
                else
                {
                    position = 2;
                }
                pos_top = now_vert + position * (-1) * pos_pl;
            }
            else 
            {
                if (now_vert <= 0)
                {                    
                    rands = mersenne() % 2;
                    if (rands != 0)
                    {
                        position = rands;
                    }
                    else
                    {
                        position = 2;
                    }
                    pos_top = now_vert + position * pos_pl;
                }
            }
            
            now_part_object_out = now_part_object + 1;
            now_block_out = now_block + 1;
            len_out = len;
            return true;
        }
        else
        {
            if (now_part_object < len)
            {
                spawn = true;
            }
            else
            {
                spawn = false;
            }

            if (spawn == true)
            {
                pos_top = now_vert;
                gener_down = -1;
                type_spawn_object = 0;
                now_part_object_out = now_part_object + 1;
                now_block_out = now_block + 1;
                len_out = len;
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
}

bool UGenerate_lvl1::GenerAngel(int now_segment,    /* Now fragment situation "Angel" x/25 */
                            float now_position, /* Now position Z-coordinates */
                            float now_position_top, /* Now position Z-coordinates for top platform */
                            float& top_position,    /* Position for spawn top platform */
                            int& down_top,  /* platform under top platform */
                            float& down_position,   /* Position for spawn down platform */
                            int& type_object_top,   /* Type object for spawn top platform */
                            int& type_object_down,  /* Type object for spawn down platform */
                            int& now_segment_out    /* Now fragment situation "Angel" x/25 */
                            )
{
    vector<float> block(5);
    float pos_pl = 80.0;
    switch (now_segment)
    { 
    case 0:
        block = {3, 0, 0, 0, 0};
        break;
    case 1:
        block = {1, 0, 1, 5, 8};
        break;
    case 2:
        block = {0, 0, 1, 7, 6};
        break;
    case 3:
        block = {0, -1, 1, 5, 0};
        break;
    case 4:
        block = {0, 0, 1, 7, 0};
        break;
    case 5:
        block = {0, -1, 1, 0, 0};
        break;
    case 6:
        block = {0, 0, 1, 0, 0};
        break;
    case 7:
        block = {0, 0, 1, 0, 0};
        break;
    case 8:
        block = {0, 0, 1, 0, 0};
        break;
    case 9:
        block = {0, 0, 1, 0, 0};
        break;
    case 10:
        block = {0, 0, 1, 0, 0};
        break;
    case 11:
        block = {0, 0, 1, 0, 0};
        break;
    case 12:
        block = {0, 0, 1, 0, 0};
        break;
    case 13:
        block = {0, 0, 1, 0, 0};
        break;
    case 14:
        block = {0, 0, 1, 0, 0};
        break;
    case 15:
        block = {0, 0, 1, 0, 0};
        break;
    case 16:
        block = {0, 0, 1, 0, 0};
        break;
    case 17:
        block = {0, 0, 1, 0, 0};
        break;
    case 18:
        block = {0, 0, 1, 0, 0};
        break;
    case 19:
        block = {0, 0, 1, 0, 0};
        break;
    case 20:
        block = {0, 1, 1, 8, 0};
        break;
    case 21:
        block = {0, 0, 1, 6, 0};
        break;
    case 22:
        block = {0, 1, 1, 8, 5};
        break;
    case 23:
        block = {0, 0, 1, 6, 7};
        break;
    case 24:
        block = {-1, 0, 0, 0, 0};
        break;
    }
    if(now_segment == 0)
    {
        top_position = now_position + pos_pl*block[0];
        down_position = now_position + pos_pl*block[1];
        down_top = block[2];
        type_object_down = block[3];
        type_object_top = block[4];
        now_segment_out = now_segment + 1;
    }
    else
    {
        top_position = now_position_top + pos_pl*block[0];
        down_position = now_position + pos_pl*block[1];
        down_top = block[2];
        type_object_down = block[3];
        type_object_top = block[4];
        now_segment_out = now_segment + 1;
    }
    
    
    return true;
}


bool UGenerate_lvl1::Generseparator(int seed,   /* Seed for generation */
                                
                                /*           FLOAT VARIABLES               */
                                float now_z,    /* Now vertical coordinates */
                                float now_z_top,    /* Now vertical coordinates top platform */
                                float coof_hard,    /* Cooficient hard-map*/
                                
                                /*          INTEGER VARIABLES              */

                                int now_block,  /* Now block this elemet */
                                int type_separator, /* Type separator (top - hard) or (down - hard) */
                                int cout_up_down_top,   /* Counter up or down spawn for top */
                                int cout_up_down,   /* Counter up or down spawn for down */
                                int expect, /* Variable for storage value for coefficient changes z-coordinates */
                                int back_separator,
                                
                                /*           FLOAT OUT VARIABLES            */

                                float& z_top,   /* Coordinates for spawn top_platform */   
                                float& z_down,  /* Coordinates for spawn down_platform */
                                
                                /*           INTEGER OUT VARIABLES          */
                                
                                int& now_block_out, /* Now block this element */
                                int& type_separator_out,    /* Type separator (top - hard) or (down - hard) */
                                int& cout_up_down_top_out,  /* Counter up or down spawn for top */
                                int& cout_up_down_out,  /* Counter up or down spawn for down */
                                int& cout_downPlat,  /* Counter platform under top_platform */
                                int& ts_platform_top,    /* Type spawn top platform */
                                int& ts_platform_down,   /* Type spawn down platform */
                                int& expect_out /* Variable for storage value for coefficient changes z-coordinates */
                                
                                )
{
    mt19937 mersenne(seed); // Set settings random generator
    int generate = 0;   // Variable for storage generate number
    float pos_pl = 80.0;   // Height block in game
    int num_up = 0;
    float up_num = 0.0;
    switch (back_separator)
    {
    case 0:
        generate = mersenne() % 100;  // Generate random number 0 or 1
        if(generate < 30)
        {
            z_top = now_z + pos_pl * 3;   // Set z-position for top platform
            z_down = now_z; // Set z-position for down platform

            now_block_out = now_block + 1;  // Increase variable now_block
            type_separator_out = 0; // Set type separator
            cout_up_down_top_out = 0;   // Set limit spawn up or down platform in top
            cout_up_down_out = 0;   // Set limit spawn up or down platform in down
            cout_downPlat = 0;  // Set value amount platforms under top
            ts_platform_top = 0;    // Type spawn top platform
            ts_platform_down = 0;   // Type spawn down platform
        }
        else
        {
            z_top = now_z + pos_pl * 3;   // Set z-position for top platform
            z_down = now_z; // Set z-position for down platform

            now_block_out = now_block + 1;  // Increase variable now_block
            type_separator_out = 1; // Set type separator
            cout_up_down_top_out = 0;   // Set limit spawn up or down platform in top
            cout_up_down_out = 0;   // Set limit spawn up or down platform in down
            cout_downPlat = 0;  // Set value amount platforms under top
            ts_platform_top = 0;    // Type spawn top platform
            ts_platform_down = 0;   // Type spawn down platform
        }
        break;
    case 1:
        generate = mersenne() % 100;  // Generate random number 0 or 1
        if(generate < 70)
        {
            z_top = now_z + pos_pl * 3;   // Set z-position for top platform
            z_down = now_z; // Set z-position for down platform

            now_block_out = now_block + 1;  // Increase variable now_block
            type_separator_out = 0; // Set type separator
            cout_up_down_top_out = 0;   // Set limit spawn up or down platform in top
            cout_up_down_out = 0;   // Set limit spawn up or down platform in down
            cout_downPlat = 0;  // Set value amount platforms under top
            ts_platform_top = 0;    // Type spawn top platform
            ts_platform_down = 0;   // Type spawn down platform
        }
        else
        {
            z_top = now_z + pos_pl * 3;   // Set z-position for top platform
            z_down = now_z; // Set z-position for down platform

            now_block_out = now_block + 1;  // Increase variable now_block
            type_separator_out = 1; // Set type separator
            cout_up_down_top_out = 0;   // Set limit spawn up or down platform in top
            cout_up_down_out = 0;   // Set limit spawn up or down platform in down
            cout_downPlat = 0;  // Set value amount platforms under top
            ts_platform_top = 0;    // Type spawn top platform
            ts_platform_down = 0;   // Type spawn down platform
        }
        break;
    case 2:
        generate = mersenne() % 100;  // Generate random number 0 or 1
        if(generate < 50)
        {
            z_top = now_z + pos_pl * 3;   // Set z-position for top platform
            z_down = now_z; // Set z-position for down platform

            now_block_out = now_block + 1;  // Increase variable now_block
            type_separator_out = 0; // Set type separator
            cout_up_down_top_out = 0;   // Set limit spawn up or down platform in top
            cout_up_down_out = 0;   // Set limit spawn up or down platform in down
            cout_downPlat = 0;  // Set value amount platforms under top
            ts_platform_top = 0;    // Type spawn top platform
            ts_platform_down = 0;   // Type spawn down platform
        }
        else
        {
            z_top = now_z + pos_pl * 3;   // Set z-position for top platform
            z_down = now_z; // Set z-position for down platform

            now_block_out = now_block + 1;  // Increase variable now_block
            type_separator_out = 1; // Set type separator
            cout_up_down_top_out = 0;   // Set limit spawn up or down platform in top
            cout_up_down_out = 0;   // Set limit spawn up or down platform in down
            cout_downPlat = 0;  // Set value amount platforms under top
            ts_platform_top = 0;    // Type spawn top platform
            ts_platform_down = 0;   // Type spawn down platform
        }
        break;
    }
    if (now_block == 0)
    {
        // If separator not start generate
        
    }
    else
    {
        if (now_block < 6)
        {
            switch(now_block)   // See string number 791-818
            {
                case 1:
                    z_top = now_z_top + pos_pl;
                    z_down = now_z;

                    now_block_out = now_block + 1;
                    type_separator_out = type_separator;
                    cout_up_down_top_out = 0;
                    cout_up_down_out = 0;
                    cout_downPlat = 1;
                    ts_platform_top = 8;
                    ts_platform_down = 5;
                    break;
                case 2:
                    z_top = now_z_top;
                    z_down = now_z;

                    now_block_out = now_block + 1;
                    type_separator_out = type_separator;
                    cout_up_down_top_out = 0;
                    cout_up_down_out = 0;
                    cout_downPlat = 1;
                    ts_platform_top = 6;
                    ts_platform_down = 7;
                    break;
                case 3:
                    z_top = now_z_top;
                    z_down = now_z - pos_pl;

                    now_block_out = now_block + 1;
                    type_separator_out = type_separator;
                    cout_up_down_top_out = 0;
                    cout_up_down_out = 0;
                    cout_downPlat = 0;
                    ts_platform_top = 0;
                    ts_platform_down = 5;
                    break;
                case 4:
                    z_top = now_z_top;
                    z_down = now_z;

                    now_block_out = now_block + 1;
                    type_separator_out = type_separator;
                    cout_up_down_top_out = 0;
                    cout_up_down_out = 0;
                    cout_downPlat = 0;
                    ts_platform_top = 0;
                    ts_platform_down = 7;
                    break;
                case 5:
                    z_top = now_z_top;
                    z_down = now_z - pos_pl;

                    now_block_out = now_block + 1;
                    type_separator_out = type_separator;
                    cout_up_down_top_out = 0;
                    cout_up_down_out = 0;
                    cout_downPlat = 0;
                    ts_platform_top = 0;
                    ts_platform_down = 0;
                    break;
            }
        }
        else
        {
            switch(type_separator)
            {
                case 0: // Top is hard
                    switch(cout_up_down)    // Generate down platform
                    {
                        case 0:
                            generate = mersenne() % 1300;
                            if (generate < 100 + coof_hard)
                            {
                                z_down = now_z;

                                ts_platform_down = 1;
                                cout_up_down_out = 0;
                            }
                            else
                            {
                                if(generate >= 100 + coof_hard && generate < 200 + coof_hard)
                                {
                                    z_down = now_z;

                                    ts_platform_down = 2;   
                                    cout_up_down_out = 0;
                                }
                                else
                                {
                                    if(generate >= 200 + coof_hard && generate < 800 + coof_hard)
                                    {
                                        num_up = mersenne() % 2;
                                        up_num = 0.0;
                                        if (num_up != 0)
                                        {
                                            z_down = now_z + pos_pl;
                                            up_num = num_up;
                                            cout_up_down_out = 1;
                                        }
                                        else
                                        {
                                            z_down = now_z + pos_pl * 2;
                                            up_num = 2.0;
                                            cout_up_down_out = 2;
                                        }  
                                        ts_platform_down = 0; 
                                    }
                                    else
                                    {
                                        if(generate >= 800 + coof_hard && generate < 1300)
                                        {
                                            z_down = now_z;

                                            ts_platform_down = 0;
                                            cout_up_down_out = 0;
                                        }
                                    }
                                } 
                            }
                            break;
                        case 1:
                            generate = mersenne() % 1150;
                            if(generate < 100 + coof_hard)
                            {
                                z_down = now_z;

                                ts_platform_down = 1;
                                cout_up_down_out = 1;
                            }
                            else
                            {
                                if(generate >= 100 + coof_hard && generate < 200 + coof_hard)
                                {
                                    z_down = now_z + pos_pl;
                                    
                                    cout_up_down_out = 2;
                                    ts_platform_down = 0;  
                                }
                                else
                                {
                                    if(generate >= 200 + coof_hard && generate < 500 + coof_hard)
                                    {
                                        z_down = now_z - pos_pl;
                                    
                                        cout_up_down_out = 0;
                                        ts_platform_down = 0;  
                                    }
                                    else
                                    {
                                        if(generate >= 500 + coof_hard && generate < 1150)
                                        {
                                            z_down = now_z;
                                    
                                            ts_platform_down = 0;  
                                            cout_up_down_out = 1;
                                        }
                                    }
                                }
                            }
                            break;
                        case 2:
                            generate = mersenne() % 1000;
                            if(generate < 500)
                            {
                                z_down = now_z;
                                    
                                ts_platform_down = 0;  
                                cout_up_down_out = 2;
                            }
                            else
                            {
                                if(generate >= 500 && generate < 1000)
                                {
                                    num_up = mersenne() % 2;
                                    if (num_up != 0)
                                    {
                                        z_down = now_z - pos_pl;
                                        cout_up_down_out = 1;
                                    }
                                    else
                                    {
                                        z_down = now_z - pos_pl * 2;
                                        cout_up_down_out = 0;
                                    }
                                    ts_platform_down = 0;  
                                }           
                            }
                            break;
                    }
                    switch(cout_up_down_top)    // Generate top platform
                    {
                        case 0:
                            generate = mersenne() % 1050;
                            if(generate < 100 + coof_hard * 1.5)
                            {
                                z_top = now_z_top + pos_pl;

                                ts_platform_top = 1;
                                cout_up_down_top_out = 1;
                                cout_downPlat = 1;
                            }
                            else
                            {
                                if(generate >= 100 + coof_hard * 1.5 && generate < 300 + coof_hard * 1.5)
                                {
                                    num_up = mersenne() % 2;
                                    up_num = 0.0;
                                    if (num_up != 0)
                                    {
                                        z_top = now_z_top + pos_pl;
                                        up_num = num_up;
                                        cout_up_down_top_out = 1;
                                        cout_downPlat = 1;
                                    }
                                    else
                                    {
                                        z_top = now_z_top + pos_pl * 2;
                                        up_num = 2.0;
                                        cout_up_down_top_out = 2;
                                        cout_downPlat = 2;
                                    }
                                    ts_platform_top = 0;  
                                }
                                else
                                {
                                    if(generate >= 300 + coof_hard * 1.5 && generate < 1050)
                                    {
                                        z_top = now_z_top;

                                        cout_up_down_top_out = 0;
                                        ts_platform_top = 0;
                                        cout_downPlat = 0;
                                    }
                                }
                                
                            }
                            
                            break;
                        case 1:
                            generate = mersenne() % 1400;
                            if(generate < 500 - coof_hard * 1.5)
                            {
                                z_top = now_z_top + pos_pl;

                                cout_up_down_top_out = 2;
                                ts_platform_top = 0;
                                cout_downPlat = 2;
                            }
                            else
                            {
                                if(generate >= 500 - coof_hard * 1.5 && generate < 700 + coof_hard * 1.5)
                                {
                                    z_top = now_z_top - pos_pl;

                                    cout_up_down_top_out = 0;
                                    ts_platform_top = 0;
                                    cout_downPlat = 0;
                                }
                                else
                                {
                                    if(generate >= 700 + coof_hard * 1.5 && generate < 1200 - coof_hard * 1.5)
                                    {
                                        z_top = now_z_top;

                                        cout_up_down_top_out = 1;
                                        ts_platform_top = 0;
                                        cout_downPlat = 1;
                                    }
                                    else
                                    {
                                        if(generate >= 1200 - coof_hard * 1.5 && generate < 1400)
                                        {
                                            z_top = now_z_top;

                                            cout_up_down_top_out = 1;
                                            ts_platform_top = 1;
                                            cout_downPlat = 1;
                                        }
                                    }   
                                }
                            }
                            
                            break;
                        case 2:
                            generate = mersenne() % 1200;
                            if(generate < 400 - coof_hard * 1.5)
                            {
                                z_top = now_z_top;

                                cout_up_down_top_out = 2;
                                ts_platform_top = 0;
                                cout_downPlat = 2;
                            }
                            else
                            {
                                if(generate >= 400 - coof_hard * 1.5 && generate < 950 - coof_hard * 1.5)
                                {
                                    num_up = mersenne() % 2;
                                    if (num_up != 0)
                                    {
                                        z_top = now_z_top - pos_pl;
                                        up_num = num_up;
                                        cout_up_down_top_out = 1;
                                        cout_downPlat = 1;
                                    }
                                    else
                                    {
                                        z_top = now_z_top - pos_pl * 2;
                                        up_num = 2.0;
                                        cout_up_down_top_out = 0;
                                        cout_downPlat = 0;
                                    }
                                    ts_platform_top = 0;  
                                }
                                else
                                {
                                    if(generate >= 950 - coof_hard * 1.5 && generate < 1200)
                                    {
                                        z_top = now_z_top;

                                        cout_up_down_top_out = 2;
                                        cout_downPlat = 2;
                                        ts_platform_top = 0;  
                                    }
                                }
                                
                            }
                            break;
                    }
                    now_block_out = now_block + 1;
                    type_separator_out = 0;
                    break;
                case 1: // Down is hard
                    switch(cout_up_down)    // Generate down platform
                    {
                        case 0:
                            generate = mersenne() % 1300;
                            if (generate < 100 + coof_hard * 1.5)
                            {
                                z_down = now_z;

                                ts_platform_down = 1;
                                cout_up_down_out = 0;
                            }
                            else
                            {
                                if(generate >= 100 + coof_hard * 1.5 && generate < 200 + coof_hard * 1.5)
                                {
                                    z_down = now_z;

                                    ts_platform_down = 2;   
                                    cout_up_down_out = 0;
                                }
                                else
                                {
                                    if(generate >= 200 + coof_hard * 1.5 && generate < 800 + coof_hard * 1.5)
                                    {
                                        num_up = mersenne() % 2;
                                        up_num = 0.0;
                                        if (num_up != 0)
                                        {
                                            z_down = now_z + pos_pl;
                                            up_num = num_up;
                                            cout_up_down_out = 1;
                                        }
                                        else
                                        {
                                            z_down = now_z + pos_pl * 2;
                                            up_num = 2.0;
                                            cout_up_down_out = 2;
                                        }  
                                        ts_platform_down = 0; 
                                    }
                                    else
                                    {
                                        if(generate >= 800 + coof_hard * 1.5 && generate < 1300)
                                        {
                                            z_down = now_z;

                                            ts_platform_down = 0;
                                            cout_up_down_out = 0;
                                        }
                                    }
                                } 
                            }
                            break;
                        case 1:
                            generate = mersenne() % 1150;
                            if(generate < 100 + coof_hard * 1.5)
                            {
                                z_down = now_z;

                                ts_platform_down = 1;
                                cout_up_down_out = 1;
                            }
                            else
                            {
                                if(generate >= 100 + coof_hard * 1.5 && generate < 200 + coof_hard * 1.5)
                                {
                                    z_down = now_z + pos_pl;
                                    
                                    cout_up_down_out = 2;
                                    ts_platform_down = 0;  
                                }
                                else
                                {
                                    if(generate >= 200 + coof_hard * 1.5 && generate < 500 + coof_hard * 1.5)
                                    {
                                        z_down = now_z - pos_pl;
                                    
                                        cout_up_down_out = 0;
                                        ts_platform_down = 0;  
                                    }
                                    else
                                    {
                                        if(generate >= 500 + coof_hard * 1.5 && generate < 1150)
                                        {
                                            z_down = now_z;
                                    
                                            ts_platform_down = 0;  
                                            cout_up_down_out = 1;
                                        }
                                    }
                                }
                            }
                            break;
                        case 2:
                            generate = mersenne() % 1000;
                            if(generate < 500)
                            {
                                z_down = now_z;
                                    
                                ts_platform_down = 0;  
                                cout_up_down_out = 2;
                            }
                            else
                            {
                                if(generate >= 500 && generate < 1000)
                                {
                                    num_up = mersenne() % 2;
                                    if (num_up != 0)
                                    {
                                        z_down = now_z - pos_pl;
                                        cout_up_down_out = 1;
                                    }
                                    else
                                    {
                                        z_down = now_z - pos_pl * 2;
                                        cout_up_down_out = 0;
                                    }
                                    ts_platform_down = 0;  
                                }           
                            }
                            break;
                    }
                    switch(cout_up_down_top)    // Generate top platform
                    {
                        case 0:
                            generate = mersenne() % 1050;
                            if(generate < 100 + coof_hard)
                            {
                                z_top = now_z_top + pos_pl;

                                ts_platform_top = 1;
                                cout_up_down_top_out = 1;
                                cout_downPlat = 1;
                            }
                            else
                            {
                                if(generate >= 100 + coof_hard && generate < 300 + coof_hard)
                                {
                                    num_up = mersenne() % 2;
                                    up_num = 0.0;
                                    if (num_up != 0)
                                    {
                                        z_top = now_z_top + pos_pl;
                                        up_num = num_up;
                                        cout_up_down_top_out = 1;
                                        cout_downPlat = 1;
                                    }
                                    else
                                    {
                                        z_top = now_z_top + pos_pl * 2;
                                        up_num = 2.0;
                                        cout_up_down_top_out = 2;
                                        cout_downPlat = 2;
                                    }
                                    ts_platform_top = 0;  
                                }
                                else
                                {
                                    if(generate >= 300 + coof_hard && generate < 1050)
                                    {
                                        z_top = now_z_top;

                                        cout_up_down_top_out = 0;
                                        ts_platform_top = 0;
                                        cout_downPlat = 0;
                                    }
                                }
                                
                            }
                            
                            break;
                        case 1:
                            generate = mersenne() % 1400;
                            if(generate < 500 - coof_hard)
                            {
                                z_top = now_z_top + pos_pl;

                                cout_up_down_top_out = 2;
                                ts_platform_top = 0;
                                cout_downPlat = 2;
                            }
                            else
                            {
                                if(generate >= 500 - coof_hard && generate < 700 + coof_hard)
                                {
                                    z_top = now_z_top - pos_pl;

                                    cout_up_down_top_out = 0;
                                    ts_platform_top = 0;
                                    cout_downPlat = 0;
                                }
                                else
                                {
                                    if(generate >= 700 + coof_hard && generate < 1200 - coof_hard)
                                    {
                                        z_top = now_z_top;

                                        cout_up_down_top_out = 1;
                                        ts_platform_top = 0;
                                        cout_downPlat = 1;
                                    }
                                    else
                                    {
                                        if(generate >= 1200 - coof_hard && generate < 1400)
                                        {
                                            z_top = now_z_top;

                                            cout_up_down_top_out = 1;
                                            ts_platform_top = 1;
                                            cout_downPlat = 1;
                                        }
                                    }   
                                }
                            }
                            
                            break;
                        case 2:
                            generate = mersenne() % 1200;
                            if(generate < 400 - coof_hard)
                            {
                                z_top = now_z_top;

                                cout_up_down_top_out = 2;
                                ts_platform_top = 0;
                                cout_downPlat = 2;
                            }
                            else
                            {
                                if(generate >= 400 - coof_hard && generate < 950 - coof_hard)
                                {
                                    num_up = mersenne() % 2;
                                    if (num_up != 0)
                                    {
                                        z_top = now_z_top - pos_pl;
                                        up_num = num_up;
                                        cout_up_down_top_out = 1;
                                        cout_downPlat = 1;
                                    }
                                    else
                                    {
                                        z_top = now_z_top - pos_pl * 2;
                                        up_num = 2.0;
                                        cout_up_down_top_out = 0;
                                        cout_downPlat = 0;
                                    }
                                    ts_platform_top = 0;  
                                }
                                else
                                {
                                    if(generate >= 950 - coof_hard && generate < 1200)
                                    {
                                        z_top = now_z_top;

                                        cout_up_down_top_out = 2;
                                        cout_downPlat = 2;
                                        ts_platform_top = 0;  
                                    }
                                }
                                
                            }
                            break;
                    }
                    now_block_out = now_block + 1;
                    type_separator_out = 1;
                    break;
            }    
        }       
    }
    return true;
}


bool UGenerate_lvl1::GenerStandart(int Seed,    /* Seed for generation */
                                
                                /*           FLOAT VARIABLES               */
                                
                                float now_z,    /* Now vertical coordinates */
                                float coof_hard,    /* Cooficient hard-map */
                                
                                /*           INTEGER VARIABLES              */
                                
                                int now_block,  /* Now block this elemet */
                                int cout_up_down,   /* Counter up or down spawn */
                                
                                /*          OUT FLOAT VARIABLES             */
                                
                                float& now_z_out,   /* Out vertical coordinates */
                                
                                /*          OUT INTEGER VARIABLES           */
                                
                                int& now_block_out, /* Now block this elemet */
                                int& cout_up_down_out,  /* Counter up or down spawn */
                                int& type_object    /* Type object for spawn */)
{
    int generate = 0;   // Variable for storage generate number
    mt19937 mersenne(Seed); // Set settings random generator
    int cout_jump = 0;  // Variable for storage generate value up or down jump
    float pos_pl = 80.0;
    switch (cout_up_down)
    {
    case 0:
        generate = mersenne() % 1200;
        if(generate < 100 + coof_hard)  // Generate spikes z = z
        {
            now_z_out = now_z;

            cout_up_down_out = 0;
            type_object = 1;
        }
        else
        {
            if(generate >= 100 + coof_hard && generate < 350 + coof_hard)   // Generate up_down jump
            {
                cout_jump = mersenne() % 3;
                switch (cout_jump)
                {
                case 0:
                    now_z_out = now_z + pos_pl * 3;

                    cout_up_down_out = 3;
                    type_object = 0;
                    break;
                case 1:
                    now_z_out = now_z + pos_pl * 1;

                    cout_up_down_out = 1;
                    type_object = 0;
                    break;
                case 2:
                    now_z_out = now_z + pos_pl * 2;

                    cout_up_down_out = 2;
                    type_object = 0;
                    break;
                }
            }
            else
            {
                if(generate >= 350 + coof_hard && generate < 450 + coof_hard)   // Generate lava z = z
                {
                    now_z_out = now_z;

                    cout_up_down_out = 0;
                    type_object = 2;
                }
                else
                {
                    if(generate >= 450 + coof_hard && generate < 1200)  // Generate ground z = z
                    {
                        now_z_out = now_z;

                        cout_up_down_out = 0;
                        type_object = 0;
                    }
                }
                
            }
            
        }
        
        break;
    case 1:
        generate = mersenne() % 1200;
        if(generate < 100 + coof_hard)  // Generate spikes z = z
        {
            now_z_out = now_z;

            cout_up_down_out = 1;
            type_object = 1;
        }
        else
        {
            if(generate >= 100 + coof_hard && generate < 350 + coof_hard)   // Generate down jump
            {
                now_z_out = now_z - pos_pl * 1;

                cout_up_down_out = 0;
                type_object = 0;
            }
            else
            {
                if(generate >= 350 + coof_hard && generate < 700 + coof_hard)
                {
                    cout_jump = mersenne() % 3;
                    switch (cout_jump)
                    {
                    case 0:
                        now_z_out = now_z + pos_pl * 3;

                        cout_up_down_out = 4;
                        type_object = 0;
                        break;
                    case 1:
                        now_z_out = now_z + pos_pl * 1;

                        cout_up_down_out = 2;
                        type_object = 0;
                        break;
                    case 2:
                        now_z_out = now_z + pos_pl * 2;

                        cout_up_down_out = 3;
                        type_object = 0;
                        break;
                    }
                }
                else
                {
                    if(generate >= 700 + coof_hard && generate < 1200)
                    {
                        now_z_out = now_z;

                        type_object = 0;
                        cout_up_down_out = 1;
                    }
                }
                
            }
            
        }
        
        break;
    case 2:
        generate = mersenne() % 1200;
        if(generate < 100 + coof_hard)
        {
            now_z_out = now_z;

            cout_up_down_out = 2;
            type_object = 1;
        }
        else
        {
            if(generate >= 100 + coof_hard && generate < 350 + coof_hard)
            {
                cout_jump = mersenne() % 2;
                switch (cout_jump)
                {
                case 0:
                    now_z_out = now_z + pos_pl * 2;

                    cout_up_down_out = 4;
                    type_object = 0;
                    break;
                case 1:
                    now_z_out = now_z + pos_pl * 1;

                    cout_up_down_out = 3;
                    type_object = 0;
                    break;
                }
            }
            else
            {
                if(generate >= 350 + coof_hard && generate < 750 + coof_hard)
                {
                    cout_jump = mersenne() % 2;
                    switch (cout_jump)
                    {
                    case 0:
                        now_z_out = now_z - pos_pl * 2;

                        cout_up_down_out = 0;
                        type_object = 0;
                        break;
                    case 1:
                        now_z_out = now_z - pos_pl * 1;

                        cout_up_down_out = 1;
                        type_object = 0;
                        break;
                    }
                }
                else
                {
                    if(generate > 750 + coof_hard && generate < 1200)
                    {
                        now_z_out = now_z;

                        cout_up_down_out = 2;
                        type_object = 0;
                    }
                }
                
            }
            
        }
        break;
    case 3:
        generate = mersenne() % 1200;
        if(generate < 100 + coof_hard)  // Generate spikes z = z
        {
            now_z_out = now_z;

            cout_up_down_out = 3;
            type_object = 1;
        }
        else
        {
            if(generate >= 100 + coof_hard && generate < 350 + coof_hard)   // Generate down jump
            {
                now_z_out = now_z + pos_pl * 1;

                cout_up_down_out = 4;
                type_object = 0;
            }
            else
            {
                if(generate >= 350 + coof_hard && generate < 700 + coof_hard)
                {
                    cout_jump = mersenne() % 3;
                    switch (cout_jump)
                    {
                    case 0:
                        now_z_out = now_z - pos_pl * 3;

                        cout_up_down_out = 0;
                        type_object = 0;
                        break;
                    case 1:
                        now_z_out = now_z - pos_pl * 1;

                        cout_up_down_out = 2;
                        type_object = 0;
                        break;
                    case 2:
                        now_z_out = now_z - pos_pl * 2;

                        cout_up_down_out = 1;
                        type_object = 0;
                        break;
                    }
                }
                else
                {
                    if(generate >= 700 + coof_hard && generate < 1200)
                    {
                        now_z_out = now_z;

                        type_object = 0;
                        cout_up_down_out = 3;
                    }
                }
                
            }
            
        }
        break;
    case 4:
        generate = mersenne() % 1200;
        if(generate < 100 + coof_hard)  // Generate spikes z = z
        {
            now_z_out = now_z;

            cout_up_down_out = 4;
            type_object = 1;
        }
        else
        {
            if(generate >= 100 + coof_hard && generate < 500 + coof_hard)   // Generate up_down jump
            {
                cout_jump = mersenne() % 3;
                switch (cout_jump)
                {
                case 0:
                    now_z_out = now_z - pos_pl * 3;

                    cout_up_down_out = 1;
                    type_object = 0;
                    break;
                case 1:
                    now_z_out = now_z - pos_pl * 1;

                    cout_up_down_out = 3;
                    type_object = 0;
                    break;
                case 2:
                    now_z_out = now_z - pos_pl * 2;

                    cout_up_down_out = 2;
                    type_object = 0;
                    break;
                }
            }
            else
            {
                if(generate >= 500 + coof_hard && generate < 1200)  // Generate ground z = z
                {
                    now_z_out = now_z;

                    cout_up_down_out = 4;
                    type_object = 0;
                }
            }
        }
        
        break;
    }
    now_block_out = now_block + 1;
    return true;
}

bool UGenerate_lvl1::ChooseChest(int Seed,
                                    int& out_chest)
{
    int generate = 0;   // Variable for storage generate number
    mt19937 mersenne(Seed); // Set settings random generator
    generate = mersenne() % 1000;
    if(generate < 80)
    {
        out_chest = 0;
    }
    else
    {
        if(generate >= 80 && generate < 700)
        {
            out_chest = 1;
        }
        else
        {
            if(generate >= 700 && generate < 1000)
            {
                out_chest = 2;
            }
        }
        
    }
    return true;
}